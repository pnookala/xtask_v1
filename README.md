# XTASK Version 1

This version of Xtask is implemented with semaphores and uses a stack for storing tasks. Fibonacci is implemented using Xtask which can be run using the below commands:

$ make

$ ./fib -f30 -w1

Here -f is for specifying which fibonacci number to calculate and -w is for number of threads to use.

