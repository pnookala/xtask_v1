#CC = gcc -O3
#CC = icc -mmic -O3	#use this for to make an executable for Phi
CC = gcc -g -DUSE_xtask
CFLAGS = -std=gnu99 -Wall -fPIC -Iinclude/
LDFLAGS = -lpthread -lomp -lm

all: libxtask-counter.a libxtask-jigstack.a xtask.so fib

libxtask-%.a: build/xtask.o build/%.o
	$(AR) rc $@ $^

xtask.so: libxtask-jigstack.a
	$(CC) $^ -shared -o $@ $(LDFLAGS)
	
#fib: build/lua.o build/ldata-write.o build/ldata-read.o libxtask-jigstack.a
fib: build/fib.o libxtask-jigstack.a
	$(CC) $^ -o $@ $(LDFLAGS)

build/%.o: src/%.c build
	$(CC) $(CFLAGS) -c $< -o $@

build:
	mkdir build

clean:
	rm -rf build *.a *.so
